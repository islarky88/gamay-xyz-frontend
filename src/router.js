import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';

Vue.use(Router);

export default new Router({
	mode: 'history',
	base: process.env.BASE_URL,
	routes: [
		{
			path: '/',
			name: 'home',
			component: Home
		},
		{
			path: '/why-us',
			name: 'why-us',
			component: () => import('./views/WhyUs.vue')
		},
		{
			path: '/solutions',
			name: 'solutions',
			component: () => import('./views/Solutions.vue')
		},
		{
			path: '/features',
			name: 'features',
			component: () => import('./views/Features.vue')
		},
		{
			path: '/pricing',
			name: 'pricing',
			component: () => import('./views/Pricing.vue')
		},
		{
			path: '/login',
			name: 'login',
			component: () => import('./views/Login.vue')
		},
		{
			path: '/signup',
			name: 'signup',
			component: () => import('./views/Signup.vue')
		}
	]
});
