import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
	state: {
		menuItems: [
			{ name: 'Why Us?', link: '/why-us' },
			{ name: 'Solutions', link: '/solutions' },
			{ name: 'Features', link: '/features' },
			{ name: 'Pricing', link: '/pricing' },
			{ name: 'Login', link: '/login' },
			{ name: 'Signup', link: '/signup' }
		],
		steps: [
			{ step: 1, icon: 'mdi-content-paste', desc: 'Paste Long Link' },
			{ step: 2, icon: 'mdi-cursor-default-click', desc: 'Press Shorten Button' },
			{ step: 3, icon: 'mdi-content-save', desc: 'Get Shortened Link' }
		]
	},
	mutations: {},
	actions: {}
});
